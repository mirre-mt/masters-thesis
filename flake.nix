{
  description = "Master's thesis - Password Compromise Monitoring Tool";
  # ref: https://git.dotya.ml/mirre-mt/masters-thesis

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    ...
  }: let
    # to work with older version of flakes
    lastModifiedDate =
      self.lastModifiedDate or self.lastModified or "19700101";
    supportedSystems = ["aarch64-linux" "x86_64-linux" "x86_64-darwin"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

    # ms, bc
    thesis = "masters";

    checkThesisType = {type}:
      if builtins.stringLength type == 0
      then "unknown"
      else type;
    thesisType = checkThesisType {type = thesis;};

    documentName = "thesis";
    pname = thesisType + "_" + documentName;
    version = "0.0.0";

    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
    nixpkgsFor = forAllSystems (
      system:
        import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        }
    );
  in rec {
    formatter = forAllSystems (
      system:
        nixpkgsFor.${system}.alejandra
    );

    overlays.default = with pkgs;
      final: prev: {
        watcher = with final;
          writeScriptBin "watch" ''
            out=".latexmkout"
            mkdir "$out"

            # initial latex compile.
            lualatex -synctex=1 -file-line-error \
              -interaction=nonstopmode -output-directory=$out thesis.tex

            # handle bibliography.
            bibtex thesis
            cp -v thesis.bbl $out

            # continuously compile latex using latexmk.
            latexmk \
              -pvc \
              -outdir="$out" \
              -pdf \
              -pdflatex="lualatex -synctex=1 -file-line-error -interaction=nonstopmode" \
              -use-make thesis.tex
            rm -r "$out"
          '';

        buildLatex = with final;
        with pkgs;
          stdenv.mkDerivation {
            inherit pname version;

            src = lib.cleanSource ./.;

            propagatedBuildInputs = with pkgs; [
              (texlive.combine {
                inherit
                  (texlive)
                  scheme-full
                  latexmk
                  bibtex
                  hyperref
                  hyperxmp
                  xmpincl
                  totpages
                  blindtext
                  fancyhdr
                  etoolbox
                  topiclongtable
                  fontspec
                  ccaption
                  ms # contains everyshi
                  ;
              })
              gnumake
            ];

            buildPhase = ''
              lualatex -synctex=1 -file-line-error \
                -interaction=nonstopmode -output-directory=$out thesis.tex

              # handle bibliography.
              bibtex thesis
              cp -v thesis.bbl $out

              latexmk \
              -pdf \
              -pdflatex="lualatex -file-line-error -interaction=nonstopmode" \
              -use-make thesis.tex
            '';
            installPhase = ''
              install -Dm444 -t $out thesis.pdf
            '';
          };
      };

    packages = with pkgs;
      forAllSystems (system: {
        inherit (nixpkgsFor.${system}) watcher buildLatex;

        default = nixpkgsFor.${system}.watcher;
      });

    apps = forAllSystems (system: rec {
      watch = {
        type = "app";
        program = "${self.packages."${system}".watcher}/bin/watch";
      };
      buildLatex = {
        type = "app";
        program = "${self.packages."${system}".buildLatex}";
      };
      default = watch;
    });

    devShells = with pkgs;
      forAllSystems (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };
        inherit overlays;

        watcher = pkgs.writeShellScriptBin "watcher" ''
          out=".latexmkout"
          mkdir "$out"

          lualatex -synctex=1 -file-line-error \
            -interaction=nonstopmode -output-directory=$out thesis.tex

          # handle bibliography.
          bibtex thesis
          cp -v thesis.bbl $out

          latexmk \
            -pvc \
            -outdir="$out" \
            -pdf \
            -pdflatex="lualatex -synctex=1 -file-line-error -interaction=nonstopmode" \
            -use-make thesis.tex

          # clean up
          rm -r "$out"
        '';
      in {
        default = nixpkgsFor.${system}.mkShell {
          name = "${pname}-" + version;

          shellHook = ''
            echo " -- in ${pname} dev shell..."
          '';

          nativeBuildInputs = with pkgs; [
            (texlive.combine {
              inherit
                (texlive)
                scheme-full
                bibtex
                latexmk
                multirow
                hyperref
                hyperxmp
                xmpincl
                ms # contains everyshi
                blindtext
                fancyhdr
                etoolbox
                topiclongtable
                fontspec
                ;
            })
            gnumake
          ];

          packages = with nixpkgsFor.${system}; [
            watcher
            # buildLatex

            # texlive.combine { inherit (pkgs.texlive) scheme-basic latexmk; }

            gnumake
            alejandra
          ];
        };
      });
  };
}
