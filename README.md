# [masters-thesis](https://git.dotya.ml/mirre-mt/masters-thesis)

this repository holds latex sources of my master's thesis with the name
[Password Compromise Monitoring Tool](https://git.dotya.ml/mirre-mt/pcmt).

besides that, there are also auxilliary files that enable easy-enough creation
of development (document writing) environments in a repeatable/reproducible
manner ([flake.nix](flake.nix)).

### LICENSE
the content of this thesis is licensed
[CC-BY-SA-2.0](https://creativecommons.org/licenses/by-sa/2.0/)
([LICENSE](LICENSE)).
